{

"Dhcp4": {
    "valid-lifetime": 4000,
    "renew-timer": 1000,
    "rebind-timer": 2000,

    "interfaces-config": {
        "interfaces": [ "*" ]
    },

    "lease-database": {
	"type": "mysql",
	"name": "kea",
	"host": "$KEA_DB_HOST",
	"user": "kea",
	"password": "$KEA_DB_PASS"
    },

	## This is now defined in the subnet4 section, on a per-network basis
    #"option-data": [
    #    {
    #        "name": "domain-name-servers",
	#    "data": "172.16.58.251, 8.8.8.8, 8.8.4.4"
	#}    
    #],
    
    # could also be 172.16.56/21
    "subnet4": [
        {
	    ## OPS network (XPP and DAU)
	    "id": 1,
	    "subnet": "172.16.56.0/21",
	    "interface": "eops0",
	    "pools": [
	    
 	        # ops-unknown
	        { "pool": "172.16.57.10 - 172.16.57.200" },
		
                #"ops-static":  "172.16.58.2  - 172.16.58.250",
		
		# ops-dau
		{ "pool": "172.16.61.10 - 172.16.61.99" }

                #ops-dau-static is 100-200
	    ],

            "option-data": [
	        {
		    "name": "routers",
		    "data": "172.16.58.147" ## via endor (as long as HZB is in shambles)
		},

		{
		    "name": "domain-name-servers",
		    "data": "134.30.95.132, 8.8.8.8, 172.16.58.251"
		},

		{
			"name": "domain-search",
			"data": "i.kmc3.physik.uni-potsdam.de"
		}

		# time-servers, domain-name-servers, domain-name, 
            ],

            "reservations": [

		# 172.16.61.0/22 DAU PCs
		{ "hostname": "ikkrukk",   "hw-address": "c8:d3:ff:b8:f8:91", "ip-address": "172.16.61.108" }, # DAU 1, XIA PC
		{ "hostname": "nacronis",  "hw-address": "9c:7b:ef:28:11:de", "ip-address": "172.16.61.109" }, # DAU 2, XAS analysis PC
		#{ "hostname": "quarmendy", "hw-address": "90:1b:0e:11:ab:55", "ip-address": "172.16.61.110" }, # DAU 3, hutch pc
		{ "hostname": "panda",     "hw-address": "70:b3:d5:2c:25:57", "ip-address": "172.16.61.111" }, # Pandabox



		# 172.16.58.0/27 (1..31): appliances
		{ "hostname": "exegol",   "hw-address": "f0:9f:c2:6d:fe:ce", "ip-address": "172.16.58.2" },   # 16-port switch
		{ "hostname": "kashyyyk", "hw-address": "F0:9F:C2:18:75:AB", "ip-address": "172.16.58.3" },   # 48-port switch
		#{ "hostname": "iego",                                        "ip-address": "172.16.56.117" }, # 24-port mikrotik switch
		{ "hostname": "icarus",   "hw-address": "8c:16:45:22:f6:c9", "ip-address": "172.16.58.11" },  # Florin
		{ "hostname": "baguio",   "hw-address": "20:c9:d0:29:d7:0f", "ip-address": "172.16.58.12" },  # Matthias' laptop

		#{ "hostname": "darkstar",  "hw-address": "??!", "ip-address": "172.16.58.146" }, # town-hall, kmc3-analysis2
		
		#{ "hostname": "endor",     "hw-address": "00:02:c9:a1:7d:e0", "ip-address": "172.16.58.147" }, # town-hall, kmc3-xpp

		{ "hostname": "aaloth",    "hw-address": "00:02:c9:08:28:de", "ip-address": "172.16.58.148" }, # town-hall, NAS
		{ "hostname": "bespin",    "hw-address": "00:02:c9:eb:6b:80", "ip-address": "172.16.58.149" }, # x-ray, kmc3-spec2
		#{ "hostname": "bespin",    "hw-address": "00:4e:01:a3:e0:c7", "ip-address": "xxxxx" },
		
		{ "hostname": "crait",     "hw-address": "00:18:7D:28:2E:6F", "ip-address": "172.16.58.153" }, # instr, kmc3-spec
		{ "hostname": "crait",     "hw-address": "00:18:7D:28:2E:6E", "ip-address": "172.16.58.154" }, # (old SPEC)
		
		#{ "hostname": "mandalore", "client-id": "mandalore-macvlan",  "ip-address": "172.16.58.155" }, # laser, kmc3-analysis3
		#{ "hostname": "mandalore", "hw-address": "00:02:C9:28:F8:FA", "ip-address": "172.16.58.155" }, # laser, kmc3-analysis3
		#{ "hostname": "mandalore", "hw-address": "48:4d:7e:9d:ed:6d", "ip-address": "...dhcp?" },

		#{ "hostname": "corellia",   "hw-address": "00:be:43:bd:4b:c6", "ip-address": "172.16.48.156" }, # town-hall, computing beast
		{ "hostname": "corellia",   "hw-address": "00:be:43:bd:4b:c7", "ip-address": "172.16.58.156" }, # town-hall, computing beast
		{ "hostname": "dagobah",   "hw-address": "00:02:c9:a1:28:10", "ip-address": "172.16.58.157" }   # ops, kmc3-analysis4, bluesky-team

	    ]
	},

        # FIXME: network id 2 should be DAU, but needs to go on the same
	# interface as OPS -- which means that we'll have 2 IPs for eops0.

        {
	    ## INSTR network
	    "id": 3,
	    "subnet": "10.128.0.0/9",
	    # availabile C-subnets:
	    #  10.250.48.x/24: explicit use in various configuration files (not here)
	    #  10.200.49.x/24: static device leases
		#  10.200.77.x/24: DAU static IPs (manual device setups)
	    #  10.0.x.x-10.199.x.x: dynamic lease pool; all IOCs will eat from here, so there should be a few.
	    # 
	    "interface": "einstr0",
	    "pools": [
 	        # instr-unknown
	        { "pool": "10.128.0.1 - 10.199.254.254" }
	    ],

            "option-data": [
	        #{
   		#    ## Default route is actually necessary because Podman will otherwise refuse
		#    ## to add a route for broadcast address (255.255.255.255), and this will
		#    ## fuck up EPICS IOC discovery. But ...147 is actually instructed to NOT
		#    ## route packages from the 10.128.0.0/9 network.
		#    "name": "routers",
		#    "data": "10.250.48.147"
		#},
	    
		{
		    "name": "domain-name-servers",
		    "data": "10.250.48.251"
		},

		{
			"name": "domain-search",
			"data": "i.kmc3.physik.uni-potsdam.de"
		}
	    ],

	    "reservations": [

		#{ "hostname": "endor",   "hw-address": "d0:8e:79:10:4a:28", "ip-address": "172.16.48.147" }, # town-hall, kmc3-xpp


		# 172.16.49.0/21, lab devices
		{ "hostname": "dasher",  "hw-address": "00:19:B3:02:06:F0", "ip-address": "10.200.49.160" }, # Stanford DG645
		{ "hostname": "dancer",  "hw-address": "00:40:9D:75:F2:CF", "ip-address": "10.200.49.161" }, # Lakeshore LS336
		{ "hostname": "prancer", "hw-address": "d4:ae:52:ce:0f:60", "ip-address": "10.200.49.162" }, # Pilatus cam server
		
		{ "hostname": "vixen",   "hw-address": "6C:62:6D:53:1B:0D", "ip-address": "10.200.49.163" }, # old BESSY mirrors PC (replaced by bracca)
		{ "hostname": "comet",   "hw-address": "9c:6b:00:00:04:b9", "ip-address": "10.200.49.164" }, # ActiveTechnologies, also 9c:6b:00:00:11:d5

		{ "hostname": "cupid",   "hw-address": "00:90:e8:94:d6:18", "ip-address": "10.200.49.165" }, # MOXA NPort 5650-8-DTL ...7686
		{ "hostname": "dunder",  "hw-address": "00:60:1a:00:25:4a", "ip-address": "10.200.49.166" }, # Agilent AWF (func. generator)
		#{ "hostname": "blixem",  "hw-address": "...", "ip-address": "172.16.49.167" },

		{ "hostname": "doc",     "hw-address": "00:19:AF:04:EE:FB", "ip-address": "10.200.49.168" }, # Rigol DG1062Z func. generator
		{ "hostname": "grumpy",  "hw-address": "00:40:F2:2C:00:30", "ip-address": "10.200.49.170" }, # 7-axis gonio
		#{ "hostname": "dopey", "hw-address": "...?", "ip-address": "172.16.49.171" }   # 1-axis table / stage
		#{ "hostname": "sleepy",  "hw-address": "...", "ip-address": "172.16.49.172" },
		{ "hostname": "bashful", "hw-address": "00:40:f2:2c:00:04", "ip-address": "10.200.49.173" },  # 4-axis slits
		{ "hostname": "sneezy",  "hw-address": "00:40:F2:39:0B:37", "ip-address": "10.200.49.174" },  # 9-axis mono

		# Eiger control computer. This beast has about half a dozen network interfaces.
		# Main access is for user1p3 interface (supposed to be "einstr0", but this isn't under
		# coreos/skycontrol management)
		# Other interfaces: user1p1 (5c:6f:69:30:80:7a), user1p2 (5c:6f:69:30:80:7b), user1p3 (5c:6f:69:30:80:78),
		#                   user2p1 100 GbT (00:62:0b:2c:67:10), user2p2 100 GbT (00:62:0b:2c:67:11)
		{ "hostname": "happy",      "hw-address": "5c:6f:69:30:80:78", "ip-address": "10.200.49.175" },

	        { "hostname": "laser-cam",  "hw-address": "00:30:53:36:1A:30", "ip-address": "10.200.49.176" },
	        { "hostname": "gonio-cam",  "hw-address": "00:30:53:31:27:79", "ip-address": "10.200.49.177" },
	        { "hostname": "fbeam-cam",  "hw-address": "00:30:53:31:27:78", "ip-address": "10.200.49.178" },
	        { "hostname": "nbeam-cam",  "hw-address": "00:30:53:31:27:7A", "ip-address": "10.200.49.179" },
	        { "hostname": "sample-cam", "hw-address": "00:30:53:2E:96:E0", "ip-address": "10.200.49.180" },

	        { "hostname": "xray-cam",   "hw-address": "00:80:ad:41:37:c5", "ip-address": "10.200.49.181" }, # old win2k?

		{ "hostname": "panda",      "hw-address": "70:b3:d5:2c:25:57", "ip-address": "10.200.49.111" }, # Pandabox

		{ "hostname": "bookworm",  "hw-address": "d8:3a:dd:8e:8b:ec", "ip-address": "10.200.49.120" },  # Raspberry Pressure Reader IOC
		{ "hostname": "sixpack",   "hw-address": "d8:3a:dd:92:fa:74", "ip-address": "10.200.49.121" },  # Raspberry Sixpack IOC
		{ "hostname": "inskkrukk", "hw-address": "00:e0:4c:68:07:a5", "ip-address": "10.200.49.122" },  # DAU 1 (instr network), XIA PC
		{ "hostname": "inscronis", "hw-address": "00:13:3b:0f:f0:26", "ip-address": "10.200.49.123" },  # DAU 2 (instr network), XAS
		{ "hostname": "quarmendy", "hw-address": "90:1b:0e:11:ab:55", "ip-address": "10.200.49.124" }   # DAU 3, hutch pc analysis PC

	    ] # INSTR reservations
	    
	} # INSTR network
	
    ] # subnets4
    
} # Dhcp4

} # EOF
